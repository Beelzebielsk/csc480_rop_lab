#!/bin/bash

############################################################
## Initial Setup: {{{
############################################################

filesLocation="../exampleFiles"
programName="badc.noprot"
program="$filesLocation/$programName"
iterations=${1:-1}
echo "Iterations: $iterations"

outputFile='/tmp/output.txt'
offsets='/tmp/offsets'

############################################################
## }}}
############################################################

# Explanation:
# 1. `coproc $program`: background $program, and create file descriptors
#    for its input and output. These fd's are in the $COPROC array.
#    - Standard output : ${COPROC[0]}
#    - Standard input : ${COPROC[0]}
# 2. `eval "exec 100<&${COPROC[0]}"`: Make ${COPROC[0]} readable under
#    file descriptor 100.  
#    - More specifically, duplicate the file descriptor for `$program`'s
#      output (as an input, since we'll be redirecting it as the
#      standard input for a later command, since we want to read it).
#      `100` becomes a duplicate of the fd ${COPROC[0]}. 
#    - This is necessary for the backgrounding of expressions that want
#      to read from ${COPROC[0]}. the $COPROC fd's don't get inherited by
#      subshells, and backgrounding happens by using subshells (the
#      backgrounded process is run in its own shell).
#    - I couldn't use FIFOs for this, because for some reason,
#      reading/writing to a FIFO changed the way that `ps` saw the
#      process, so I could no longer inspect the stack pointer of the
#      process. coprocs don't have this drawback, though I don't
#      understand why it occurs in the first place.
# 3. `cat <&100 > output.txt`: Read the contents of fd 100 and print
#    them (specifically, I'm making the stdin of `cat` a duplicate of
#    the stdout of fd 100, which is much like a pipe). Then, print those
#    contents into the file `output.txt`, so that I can retrieve them
#    later.
# 4. `eval "echo '' >&${COPROC[1]}`: Write '' (nothing) to the stadnard
#    input of $program. In more detail, make the stdout of the `echo`
#    command a duplicate of the stdin of $program, so that when data is
#    written to stdout of echo, it is written the stdin of $program.

coproc $program
#echo ${COPROC[*]}
sp=$(ps -o esp --no-header -C badc.noprot)
eval "exec 100<&${COPROC[0]}"
cat <&100 > $outputFile &
eval "echo '' >&${COPROC[1]}"

bufLoc=$(sed -nr -e 's/.*0x([[:xdigit:]]+).*/\1/p' < $outputFile)
# From the first charater to the character that's ${#sp} characters away
# from the end. So this takes the highest digits of $bufLoc that are not
# in $sp.
higherBytes=${bufLoc:0:-${#sp}}
sp=$(printf "%016x" "0x$higherBytes$sp")
# It's easier to read the stack pointer of a running program than the
# get the location of a specific buffer in the program, due to tools
# like ps. So we want this difference because (sp + (bufloc - sp)) =
# bufloc. We can figure out where the buffer location is using this
# offset, because the offset doesn't change.
echo "Offset: $(( 0x$bufLoc - 0x$sp  ))"

echo "ESP: $sp"
echo "Buffer location: $bufLoc"

#rm $outputFile $offsets

# TODO:
# - Figure out a way to get a file descriptor that's definitely not in
#   use, instead of just guessing '100'
# - How do I find the buffer location if I don't run the program from
#   within the script? Can I have a script manage two shell sessions? Is
#   there something that I can do with forking or something?
