#!/bin/bash
# This examines the 64-bit location of the 'name' buffer during
# invocations of the 'badc' program. It checks if the address of the
# 'name' buffer (and by extension, the location of the stack) is
# constant from invocation to invocation. 
# In particular, it examines the first 32 bits to see what's in there,
# and examines the higher bits to see what's in there. The reason for
# this behavior is that, typically the first 32-bits are constant, but
# the higher bits are not.

# Accepts one positional argument: number of iterations

# Usage: ./examine_buf_locations [iterations]

iterations=${1:-100}
#echo "Num iterations: $iterations"
program=${2:- "../exampleFiles/badc.noprot"}
outFile=${3:- "../exampleFiles/stacks.txt"}

declare -A suffixesCount
#printf "pointer | last few | least eight" > $outFile
printSpec="%-18s | %-10s | %-10s\n"
printf "$printSpec" pointer "last few" "least eight" > $outFile
for i in $(eval echo {1..$iterations}); do
	bufLoc=$(echo "" | $program | sed -nr -e 's/0x[[:digit:]]+/&/p')
	#echo "Full stack pointer: $stackPtr"
	lastFew=${bufLoc:0:-8}
	leastEight=${bufLoc: -8}
	if [[ ! -v suffixesCount[$lastFew] ]]; then
		echo "Not."
		suffixesCount[$lastFew]=1
	else
		suffixesCount[$lastFew]=$((suffixesCount[$lastFew] + 1));
	fi
	printf "$printSpec" $bufLoc $lastFew $leastEight >> $outFile
done

echo "Summary" >> $outFile

for suffix in ${!suffixesCount[@]}; do
	echo "$suffix: ${suffixesCount[$suffix]}";
	echo "$suffix: ${suffixesCount[$suffix]}" >> $outFile
done

