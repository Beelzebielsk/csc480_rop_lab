# Notes on System Calls

## execve : What is it good for?

t will execute an executable file. The first argument is the filename
of the executable.

### Arguments

- RDI : Pointer to the string of the filename for the program to be
	executed.
- RSI : Pointer to `argv` string. *Will succeed even with this null*.
- RDX : Pointer to `envp` string *Will succeed even with this null*.

We can just null out the second and third arguments to make calling it
simple.

# Notes on Programs

## xxd

Produces readable bytes from binaries, or turns readable bytes into
binaries.

### Options

- `-s` : *Offset*. An offset into the file. The manual calls it a
	location to seek to within the file. Can also be relative to a
	current position in the file if reading from stdin. Relative offsets
	are otherwise meaningless.
- `-l` : *Length*. An amount of octets (basically a byte) to write
	to output.
- `-p` : *Plain output style*. Dicates the output style of the
	hexdump.
- `-r` : *Reverse dump*. Turns a dump back into a binary string. So if
	you were to run "cat \$file | xxd -p | xxd -p -r", you'd get the file
	back.

## setarch

Sets the architecture for an object file. The command that the guy
uses specifically is:

	setarch `arch` -R ./victim

Where `./victim` is some object file for a `C` program with a buffer
that's just ripe for the picking. The ``arch`` is supposed to be process
substitution, and that process is likely just going to output the
architecture of the current computer.

The architecture for my computer is most probably `linux64`. If that's
not it, then it's `x86_64`.

NOTE: setarch will run the program in an environment such that ASLR is
disabled. It's not something that you run on the program to permanently
change it.

### Options

- `-R` : Disables Address Randomization.

## ps

This allows you to view information about a program. You get to look at
important details about the program, such as who's running it, and
details about it's state.

### Options

There are a bunch here, but some of note:

- `o` : *Outputs a list of fields*. The fields are specified right after
	typing `o`, without a hyphen in front of it. One field in particular
	is the `esp` field which prints the stack pointer of the of the
	program. Unfortunately, it only prints the first four bytes of the
	stack pointer, given that `esp` references  a 32-bit register. If need
	be, we can also inspect the `eip` register to figure out what
	instruction is currently being executed.

# ASM Notes

- the `.octa` directive seems to mean an eight byte memory region.
- The registers in the argument list for system calls has an order:
	positional arguments. So first in the list is the first argument to
	the system call and so on.
	1. RDI
	2. RSI
	3. RDX
	4. R10
	5. R8
	6. R9

# Notes

## The Shell Game

The addresses he has for his dump are different from mine. Not sure why
that is.  He uses the `objdump` program to figure out where the custom
assembler stuff lives inside of the `shell.c` program, then he uses xxd
to get the pure hex out of the object file.

I read from where the object file starts (`needle0`) to where the
object file ends `(needle1)`, and apparently I should read out to the
nearest multiple of 8.

However, the length of the program is the same: **29 Bytes**.

## The Three Trials of Code Injection

As of yet, can't get my hands on the `execstack` program. However, as a
close second, I can compile a program with an executable stack. The
linker can produce a file with an executable stack, and I can make `gcc`
pass options directly to the linker. The linker option that I would use
to pass the keyword is `-z` and the keyword is `execstack`. The same
option exists in gcc, and it'll pass keywords directly to the linker
using thhe same option. So I can compile programs with:

	gcc ... -z execstack

To make the stack executable.

To put the number it little endian, he firsts prints it as an eight byte
hex number by setting the width to 16 using printf, then he uses `tac` to
print standard input in reverse. Normally `tac` would print lines in
reverse, but he changes the separator to be 'every two charaters', thus
printing every set of two characters in reverse.

Alternative to reversing:

	`printf "%016x\n" 0xdeadbeef |
		sed -r -e 's/[^%]+/%&/; :start s/([^%]*)%(.*)(..)/\1\3%\2/p; t start s/%$//;'`

The substitutions are basically productions in a markov language.

Supposing that the `name` buffer is right after the saved RBP, and
that's right after the return address, we have exactly 80 bytes to
overwrite (64 bytes, then 8, then 8 = 80 bytes).

- We can write out the shellcode for 29 (round up to 32) bytes.
- From there we can zero out the remaining bytes of name and an
	additional eight bytes for saved base pointer. That's 32 bytes
	remaining from the `name` buffer (64 - 32 = 32), and 8 more bytes for
	the saved RBP, which gives us 40 bytes.
- Finally, we can replace the last eight bytes with the address of the
	beginning of the buffer, thus causing the program to return into the
	stack and execute code there.

So that looks like:

```
saved Return Addr | ESP + 72 .. + 79
saved RBP         | ESP + 64 .. + 71
name[63]          | ESP + 63
...               |
name[0]           | ESP
Stack Top:
```

Next, with `xxd`, we can turn a hexdump back into binary format. We'll
use it with the 80 bytes that we specified to create a binary string to
feed into our function.

### Why do we use the `cat` command?

As best as I can tell, from what I've researched, we use the `cat`
command to keep the standard input open. Without it, we would succesfuly
write all the exploit code in the buffer, but as soon as we were done,
we could no longer type anything into the program. So this is why the
`cat` command is there, by itself.

## The Importance of Being Patched

Once we move onto getting the stack pointer of the process, we hit a
problem: we can only inspect the first four bytes of it (on a 64-bit
machine, it's eight bytes, though several of the last bytes may be
`0x00`).

The report suggests that the last part of the stack pointer is always
going to be `0x7fff`. However, on my machine, that's not true. I
inspected the printed address of the `name` buffer 20000 times and it
comes up with various suffixes (the script
`helperScripts/examine_buf_locations.sh` performs this function).

- `0x7ffe`: 5046
- `0x7ffd`: 4935
- `0x7fff`: 5038
- `0x7ffc`: 4981

Each of which with about a 25% chance of occuring. This leads me to
believe that it's probably on purpose and may be part of the ASLR that's
implemented on my machine.

Using the guide's process, that cuts our chance of success by about 75%,
since we're essentially guessing the actual location of the stack
pointer by appending `0x7fff` to the pointer that we read from `ps`.

Up to this point, I've been unable to find a replacement for `ps` that
would allow me to read bytes above the 32-bit mark.

However, after reading later on in the tutorial, I found a way to get
the full stack pointer by taking the most siginificant bytes of of the
addresses for the stack from the `/proc/$pid/maps`, where `$pid` is the
process id of the program to be exploited.

The offset is also different on my machine that the offset given in the
tutorial. It's 120. Still not sure why that is.

### Using Named Pipes

Automating this is tough. However, I've come to a solution using the
`coproc` command. I might have been able to get away with fifo's only,
but I didn't realize that there's a difference between redirecting a
fifo into a program and piping the fifo into that program. For some
reason, on a redirect, you can't examine the program anymore because the
name of the executable is the name of the shell within which it's
running. On a pipe, the executable name is not touched. Pretty weird.

Some notes:

- When you use a FIFO and redirect the FIFO into the exploitable
	program, you can no longer see the program under it's normal name when
	using `ps`, and you can't examine it's stack pointer. To keep things
	normal, follow the directions of the document *exactly* and type `cat
	<FIFO> | <program>`.
- When you finally do get it working, you need to type the commands into
	the shell that's writing to the FIFO, since the standard input of the
	exploited program is reading from the FIFO, not from keystrokes.
- The indicator that it worked in my computer is pressing enter a few
	times on the shell writing to the FIFO doesn't cause the program to
	exit. The program shouldn't exit for as long as the FIFO is being
	written to, and the FIFO is being written to for as long as the other
	program hasn't closed. So if the program writing to the FIFO closes
	quickly, that means that the other program had a segfault.

## Executable Space Perversion

Why does `RET` allow us to do what it does?

Well, remembering how `RET` normally works, it has to pop a return
address off of the stack, and jumps to that address. Since we're not
executing matching `call` instructions, it keeps popping new return
addresses off of the stack.

So with each next call to `RET` (which is going to be at the end of each
gadget), we pop off the next return address that we specified on the
stack. The gadgets get executed in that order. So the end of one gadget
causes another gadget to start execution.

## Go Go Gadgets

### Correctness of Byte Sequence

Confirming that the byte sequence `0x5f 0xc3` corresponds to the
instructions `pop %rdi retq`.

Excerpts from `shell_disassembly.txt`:

```
666:	5f                   	pop    %rdi
697:	c3                   	retq
```

### Finding a Gadget

We can find a gadget by figuring out a sequence of bytes that
corresponds to a set of instructions that we'd like to execute, and then
jump to the beginning of that set of bytes.

If the set of bytes is in a library, then we have to figure out where in
the library the bytes are, and where in the program's memory does the
library start; the absolute address of those bytes will be the library
offset plus the library start.

To figure out the offset, we need to search through the hexdump of the
library and look for a good byte sequence. I chose to do this by using
the same dumping command as the tutorial:

	xxd -c1 -p libc.so.6
	-c1 : One byte per line (number of columns)
	-p : plain output

Then searching through it using vim for the proper byte sequence.

it seems that a location of these bytes in libc.so.6 is `1fc1a`.

Line numbers start at 1, and using `-c1` causes one byte to be printed
per line. The 1^st^ line is the 0^th^ byte, and in general, the i^th^
line coressponds to the (i-1)^th^ byte.

So we have the location of a gadget, relative to the start of
`libc.so.6`. We now need an absolute location in `libc.so.6`, preferably
the start of it.

### Finding the Start of a Library

To find the start of a library, we can consult a file which keeps track
of how libraries are mapped into the address space of a process. This
file is kept in `/proc/<process pid>/maps`.

> Not only are maps for libraries kept there, you can also find the
> addresses for the heap and the stack. This same process can be used to
> figure out the last four hex digits for the stack, which can help some
> of my scripts for figuring out the correct larger bytes for the stack
> pointer.

### Working out the sizes of stuff:

For reference, the stack of this program:

```
saved Return Addr | *name + 72 .. + 79
saved RBP         | *name + 64 .. + 71
name[63]          | *name + 63
...               |
name[0]           | *name
```

After figuring out the location of the `name` buffer (which is no longer
that hard), we have the space between `&name[0]` and `&name[71]` (72
bytes) to put stuff. It has to be filler, but we can also put strings
there that we'll need. Starting from position 72, we can start putting
addresses in memory for gadgets.

So our input to the program is probably going to look something like:

/bin/sh          | 7 Bytes, `&name[0]`, `exploitString`
...              | ...
0                | ...
...              | ... 72 Bytes Total
`pop %rdi`       | 8 Bytes
`&exploitString` | 8 Bytes
`&system()`      | 8 Bytes

It should return to the `pop %rdi` gadget by popping the address of the
gadget into `%rip`, then it'll perform a `pop`, which will pop the
address of the `/bin/sh` into `%rdi`. Returning from this gadget will
cause us to jump into the `system()` function.

### Writing the Exploit Code

piping from echo to `xxd -p` will encode newlines from your string.
However, `xxd -r` is not sensitive to whitespace. So be careful to
eliminate newlines from getting into your exploitBytes.

### Gadget List

- Setting Registers:
	- `pop %rax`, `0x58 0xc3`,     lineno: 221209
	- `pop %rbx`, `0x5b 0xc3`,     lineno: 203401
	- `pop %rcx`, `0x59 0xc3`,     lineno: 186196
	- `pop %rdx`, `0x5a 0xc3`,     lineno: 7059
	- `pop %rsi`, `0x5e 0xc3`,     lineno: 129995
	- `pop %rdi`, `0x5f 0xc3`,     lineno: 130075
	- `pop %rbp`, `0x5d 0xc3`,     lineno: 129396
	- `pop %r8`, `0x41 0x58 0xc3`,  lineno: N/A
	- `pop %r9`, `0x41 0x59 0xc3`,  lineno: N/A
	- `pop %r10`, `0x41 0x5a 0xc3`, lineno: 1003493
	- `pop %r11`, `0x41 0x5b 0xc3`, lineno: N/A
	- `pop %r12`, `0x41 0x5c 0xc3`, lineno: 132074
	- `pop %r13`, `0x41 0x5d 0xc3`, lineno: 129739
	- `pop %r14`, `0x41 0x5e 0xc3`, lineno: 129994
	-	`pop %r15`, `0x41 0x5f 0xc3`, lineno: 130074


The location of `system` in libc is called `__libc_system`. Using the
`nm` utility reports that location as `system`. Not sure why there's a
difference in the names.

# Standing Questions organized by section

## Some Assembly Required

- Why does the assembly snippet (the shellcode) load a pointer to the string
	"/bin/sh" into `RDI`? How is that possible? I'd argue that it's
	because the string, being data, is at the end of the data section,
	which is right under the stack, and if the stack is empty, then that
	string is on the top of the stack, but that should only be true when
	the program first starts. Why is this snipped portable?

## The Shell Game

- Why does the guy's disassembly start at 0x40_00_00, and mine starts
	at 0x4f8?
- Why am I dumping out a number of bytes from the object file that's a
	multiple of 8? Is 8 a special number here? Is it for alignment
	purposes? Or just to make the stack arithmetic easier?

## Three Trials of Code Injection

Why does the `gets` function read beyond the null bytes in our exploit
code? Is the delimiter character strictly a newline?

## Go Go Gadgets

If we have to put '/bin/sh' in our buffer, do we have to follow it with
a null byte?

It's not terribly important here, because we only have the one string,
and the spacer that we use is all null-bytes, so the string is going to
be null-terminated anyhow, but if we have more than one string, that
would be pretty important.

How do I use the `maps` file to find the beginning of a library that's
been mapped into the program?

> My hunch is to look for the region with an offset of 00000 for a
> library. The start of that region ought to be the start of the
> library.

> Can you use a gadget if it's not mapped into memory? If the memory
> region is required, will it get mapped into memory? Is it possible to
> influence which regions get mapped into memory?

> Is there some standard size for these regions? Are they multiples of a
> page, at least?

Why does the first way not work, but the second way does?

# TODO

- Figure out more valuable byte sequences to make into gadgets.
	- Interesting System Calls:
	- Arithmetic on registers
- What privileges can we take away to make this harder? How do we make
	the attack succeed under these conditions, or at least boost our
	chances?
	- Access to the `maps` file. Seems the owner of the file is the user
		that ran the process. If we were an adversary, then we wouldn't have
		access to this file.
		- In that case, how do we still obtain a lost of the snooped
			information?
	- The ability to run `ps`. We might not be able to run anything on the
		machine which has the program. This might be a service on a remote
		machine somewhere else, and we'll only be able to pass a string to a
		function.
	- Access to the source code. What would we do if we didn't know the
		size of the buffer?
- Is there a gadget that can be run an arbitrary number of times without
	changing the state of the stack? Perhaps `push`? We'd push, then
	return, then push, then return. However, we'd effect the location to
	which we'd return based on what we're pushing. Also, we probably have
	no control over what we're pushing if the `push` gadget is the first
	gadget that we run.
- Is there a way to snoop on the value of a stack canary? Is stack
	protection an absolute protection against ROP, or is there a way to
	overwrite a canary with the same value, which would allow you to smash
	the return address undetected?

# GDB Notes

GDB can't always help with disassemblings because it gets the
disassembly from the actual files/libraries themselves, and the gadgets
often end up jumping in between instructions.

For example, the particular `pop %rdi` gadget that I chose comes from
jumping into the instruction  	`pop %r15`, followed by `ret`. This is
because their representations are:

- `pop %rdi`, `0x5f 0xc3`,     lineno: 130075
-	`pop %r15`, `0x41 0x5f 0xc3`, lineno: 130074

So, you can jump into the second byte of a `pop %r15` instruction which
is followed by a `ret` instruction to get a `pop %rdi` gadget.  The
disassembly would just show you the original `pop %rdi` instruction.

Apparently, you never actually exit from the `do_system` function call
within the `system` function. The shell activates within the `do_system`
call. So what is `do_system`? It's also within this function that the
normal version of the rop program (where the '/bin/sh' is inside of the
`name` buffer, as opposed to being after the return address) that the
exploited program hits a segmentation fault.
